<?php
#print_r(PDO::getAvailableDrivers());
$host = "localhost";
$dbname = "atomic_project_b37";
$user = "root";
$pass = "";
$table_name = "book_title";
try {

    $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
    $DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    $STH = $DBH->prepare("SELECT * from book_title");
    $STH->execute();

    echo "success<br>";
}
catch(PDOException $e){
    echo "sorry";
    echo $e->getMessage();
    file_put_contents("PDOerr.txt",$e->getMessage()."\n",FILE_APPEND);
}

$STH->setFetchMode(PDO::FETCH_ASSOC);

while($row = $STH->fetch()){
    echo "using ASSOC mode<br>";

    echo $row['id'] . "<br>";

    echo $row['book_title'] . "<br>";

    echo $row['author_name'] . "<br>";
    echo "<br>";
}

$STH = $DBH->prepare("SELECT * from book_title");
$STH->execute();

$STH->setFetchMode(PDO::FETCH_OBJ);

while($row = $STH->fetch()){
    echo "using Object mode<br>";
    echo $row->id ."<br>";
    echo $row->book_title ."<br>";
    echo $row->author_name ."<br>";
    echo "<br>";
}


