<?php

$host = "localhost";
$dbname = "atomic_project_b37";
$user = "root";
$pass = "";
$table_name = "book_title";
try {
    $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
    $DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    $data = array('book_title'=>'PHP','author_name'=>'Tanim Mushfik');
    $STHinsert = $DBH->prepare("INSERT INTO book_title (book_title, author_name) value (:book_title,:author_name)");
    $STHinsert->execute($data);

    echo "success<br>";
}
catch(PDOException $e){
    echo "sorry";
    echo $e->getMessage();
    file_put_contents("PDOerr.txt",$e->getMessage()."\n",FILE_APPEND);
}

$STH = $DBH->prepare("SELECT * from book_title");
$STH->execute();


$STH->setFetchMode(PDO::FETCH_OBJ);

while($row = $STH->fetch()){
    echo "using Object mode<br>";
    echo $row->id ."<br>";
    echo $row->book_title ."<br>";
    echo $row->author_name ."<br>";
    echo "<br>";
}